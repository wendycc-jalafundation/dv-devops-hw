# **Homework**
### 1. Virtual Box installation. Fedora and Ubuntu virtualization
    This homework was made locally
### 2. CLI Tool for Virtual Box
Link to the Project [`Devops-VBoxCLITool`](/HW-Class%20II.md)
### 3. SSH keys and Jenkins
Link to [`My solution`](/HW-Class%20III.md)

### 4. Configure a container for node and the Dose project
Link to [`My Solution`](HW-4%20Install%20Node-Dose%20on%20Vagrant-Docker.md)
### 5. Install Node on Jenkins Agents
Link to [`My solution`](HW-5%20Install%20Node%20on%20Jenkins%20Agents.md) and my `step 1` branch for the dev-environment project
### 6. Jenkins Pipeline for Dose
Link to [`My solution`](HW-6%20Jenkins%20Pipeline%20for%20Dose.md) and [`Forked Dose Project`](https://github.com/wendycc-jf/Dose/tree/enable_ci)