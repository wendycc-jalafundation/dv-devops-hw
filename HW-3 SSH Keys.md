# Assignment
1. Review what are SSH public/private keys
2. Create a personal user in both VMs
3. Setup SSH keys for the personal user
4. Login to the VM with your personal user without using a password
5. Install Jenkins in both VMs using docker
6. Document the steps

# Solution

## Creating a personal user (with sudo permissions)
1. Creating a personal `user`
    ``` sh 
    $ useradd <user>
    ```
2. Assigning a password
    ```sh 
    $ passwd <user>
    ```

3. Adding the `user` to the `wheel` group
    ```sh
    $ usermod –aG wheel <user>
    ```
## Setting up SSH keys for the user
1. Logged in with `user`
2. Generating the keys
    ```sh
    $ ssh-keygen -t rsa
    ```
3. Hit `Enter` to save the keys in the default location
    ```sh
    Enter file in which to save the key (/home/<user>/.ssh/):
    ```
4. Copy the public key to the server we want to connect to
    ```sh
    $ ssh-copy-id <user>@server ip
    ```
5. Logging in to server without password
6. Repeat this steps in all 3 machines: Local, Fedora and Ubuntu
7. After that we can delete the password from the personal users in the Ubuntu and Fedora VM. This way we are forced to use the authentication keys
    ```sh
    $ passwd -d <user>
    ```
    
