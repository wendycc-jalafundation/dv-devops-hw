# Assignment
Execute a jenkins pipeline for the Dose project

# Solution
## Vagrant File
```groovy
pipeline {
    agent any

    environment {
      mainServerImageName = "jalafoundation/dose-main-server"
      contentServerImageName = "jalafoundation/dose-content-server"
      databaseServerImageName = "jalafoundation/dose-database-server"
      registryCredential = '4f2fcb8d-b172-4173-9f71-67c36b6addaf'
      dockerImage = ''
    }

    stages {
        stage('Cloning Git') {
          steps {
            git([url: 'https://github.com/wendycc-jf/Dose.git', branch: 'enable_ci'])
          }
        }
        stage('Build Database Server') {
            steps {
                echo 'Building content server...'
                script {
                  dockerImage = docker.build(databaseServerImageName, "./DatabaseServer")
                }
            }
        }
        stage('Build Main Server') {
            steps {
                echo 'Building main server...'
                script {
                  dockerImage = docker.build(mainServerImageName, "./MainServer")
                }
            }
        }
        
        stage('Publish Main Server') {
            steps {
              script {
                docker.withRegistry( '', registryCredential ) {
                  dockerImage.push("$BUILD_NUMBER")
                   dockerImage.push('latest')
                 }
               }
            }
        }
        stage('Build Content Server') {
            steps {
                echo 'Building content server...'
                script {
                  dockerImage = docker.build(contentServerImageName, "./ContentServer")
                }
            }
        }
        stage('Publish Content Server') {
            steps {
              script {
                docker.withRegistry( '', registryCredential ) {
                  dockerImage.push("$BUILD_NUMBER")
                   dockerImage.push('latest')
                 }
               }
            }
        }
         
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
        stage('Cleanup') {
            steps {
                sh "docker rmi $mainServerImageName:$BUILD_NUMBER"
                sh "docker rmi $mainServerImageName:latest"
                sh "docker rmi $contentServerImageName:$BUILD_NUMBER"
                sh "docker rmi $contentServerImageName:latest"
            }
        }
    }
}
```
## Configurations over the original project
### Main Server
*No changes*
### Content Server
1. Dependecies fixed
2. Credenciales for the DB were updated
### Database Server (*added directory*)
1. A dockerfile was created to build an image with Postgres
2. The db_schema.sql files from the Content/Main Server were moved here so they can be used during the image building

## Pipeline executed:
![](HW-6.png)

## Complete project
Link to the [forked Dose project](https://github.com/wendycc-jf/Dose/tree/enable_ci)
