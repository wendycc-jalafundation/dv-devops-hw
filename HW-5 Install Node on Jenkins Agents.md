# Assignment
Install Node on the Jenkis Agents

# Solution
Mainly using the command:  ``sudo -H -u jenkins bash`` to install node on the agent using the user `jenkins`
## Vagrant File
```ruby
[...]

(1..NODE_COUNT).each do |i|
    config.vm.define "agent-#{i}" do |agentConfig|
      agentConfig.vm.box = BOX_IMAGE
      agentConfig.vm.hostname = "agent-#{i}"
      agentConfig.vm.network :private_network, ip: "10.0.0.#{i + 10}"
      agentConfig.vm.provider :virtualbox do |vb|
        vb.gui = false
        vb.memory = "2048"
        vb.cpus   = "1"
      end
      #Configure node for each agent
      config.vm.provision "install-nvm", type: "shell", inline: <<-SHELL
      sudo -H -u jenkins bash -i -c "curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash"
      sudo -H -u jenkins bash -i -c "source ~/.profile"
      sudo -H -u jenkins bash -i -c "nvm install node"
      SHELL
    end
 end

[...]
```
## Project
Link to the project with the modified Vagrant File

https://gitlab.com/wendycc-jalafundation/devops-dev-environment/-/blob/step1/ubuntu-focal64/Vagrantfile
