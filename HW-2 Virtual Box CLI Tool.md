# Assignment
Using the VirtualBox SDK of the language you choose, create a CLI tool to 
manage your virtual machines

     https://www.virtualbox.org/wiki/Downloads
     https://pypi.org/project/pyvbox/

* The tool should
    * Create a VM
    * Delete a VM
    * Stand up a VM
* Push to Git

# Solution
My VirtualBox CLI Tool https://gitlab.com/wendycc-jalafundation/VBoxCLITool
