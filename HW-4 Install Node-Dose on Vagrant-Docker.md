# Assignment
Create a Docker container that includes Node and the Dose project in the VM (vagrant)

# Solution
Two `provision` methods were used

1. The SHELL provision will clone the Dose project
2. The `git-dose` provision will build the image using the existing docker file that it's found inside the `./MainServer`  

## Vagrant File
```ruby
 config.vm.provision "shell", inline: <<-SHELL
     apt-get update
     apt-get install -y apache2 net-tools git
     mkdir /NodeProject
      cd /NodeProject
     git clone https://github.com/GustavPS/Dose.git

   SHELL
  
   config.vm.provision "git-dose", type:"shell", 
    inline: <<-SHELL
      cd '/NodeProject/Dose/Main Server'
      docker build -t dose .
      docker run -dp 3000:3000 dose
      SHELL
```
